#ifndef _TESTS_H_
#define _TESTS_H_

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <sys/mman.h>

enum {
    MAX_MALLOC_LENGTH = 8
};

struct test_data {
    void*     heap;
    size_t    heap_size;
    uint8_t*  malloc[MAX_MALLOC_LENGTH];
    size_t    malloc_length;
    size_t    malloc_size;
};

void run_tests();
void test_memory_allocate();
void test_free_one_in_several();
void test_free_two_in_several();
void test_allocate_huge_memory();
void test_allocate_another_place_memory();


#endif // _TESTS_H_
